//
//  ProfileController.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileController: UIViewController {
    enum Segues: String {
        case view = "viewSegue"
    }

    @IBOutlet weak var totalMediaLabel: UILabel!
    @IBOutlet weak var totalFollowersLabel: UILabel!
    @IBOutlet weak var totalFollowsLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var bioTextView: UITextView!

    var userInfoViews: [UIView] = []
    var shouldBlur = true

    override func viewDidLoad() {
        super.viewDidLoad()

        self.avatarImageView.layer.masksToBounds = true
        self.avatarImageView.layer.cornerRadius = 40

        self.userInfoViews.append(contentsOf: [self.totalMediaLabel,
                                               self.totalFollowersLabel,
                                               self.totalFollowsLabel,
                                               self.usernameLabel,
                                               self.avatarImageView,
                                               self.bioTextView, ])

        InstAPI.shared.selfInfo { info, error in
            guard let model = UserInfoViewModel.instance(with: info) else {
                let alert = UIAlertController.init(title: "Error", message: "Failed to get user info from the server: \(error)", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                return
            }

            self.usernameLabel.text = model.username
            self.totalMediaLabel.text = model.totalMedia
            self.totalFollowersLabel.text = model.totalFollowers
            self.totalFollowsLabel.text = model.totalFollows
            self.bioTextView.text = model.bio
            
            self.avatarImageView.kf.indicatorType = .activity
            self.avatarImageView.kf.indicatorType = .custom(indicator: GlowIndicator())
            self.avatarImageView.kf.setImage(with: model.avatar)

            let _ = self.userInfoViews.map { $0.unblurIt() }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.shouldBlur {
            let _ = self.userInfoViews.map { $0.blurIt() }
        }

        self.shouldBlur = false
    }

    func selectMediaAction(_ sender: Any?) {
        self.performSegue(withIdentifier: Segues.view.rawValue,sender: sender)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Segues.view.rawValue {
            let ctrl = segue.destination as! ViewController
            let media = sender as! InstMedia
            
            ctrl.media = media
        }
    }
}
