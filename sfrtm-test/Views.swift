//
//  Views.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Kingfisher

extension UIView {
    func blurIt() {
        let layer = CALayer()
        layer.frame = self.layer.bounds
        layer.name = "blurLayer"
        self.layer.addSublayer(layer)

        let anim = CABasicAnimation(keyPath: "backgroundColor")
        let from: Float = 0.95
        let to: Float = 0.98

        anim.repeatCount = .infinity
        anim.fromValue = UIColor(colorLiteralRed: from, green: from, blue: from, alpha: 1.0).cgColor
        anim.toValue = UIColor(colorLiteralRed: to, green: to, blue: to, alpha: 1.0).cgColor
        anim.duration = 0.7
        anim.autoreverses = true

        layer.add(anim, forKey: "anim")
    }

    func unblurIt() {
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1.0
        })

        for layer in self.layer.sublayers ?? [] {
            if layer.name == "blurLayer" {
                layer.removeFromSuperlayer()
            }
        }
    }
}

struct GlowIndicator: Indicator {
    let view: UIView = UIView()

    func startAnimatingView() {
        self.view.blurIt()
        self.view.isHidden = false
    }

    func stopAnimatingView() {
        self.view.unblurIt()
        self.view.isHidden = true
    }
    
    init() {
        view.backgroundColor = .red
    }
}

class UserInfoViewModel {
    let userInfo: InstUserInfo

    init(_ userInfo: InstUserInfo) {
        self.userInfo = userInfo
    }

    static func instance(with info: InstUserInfo?) -> UserInfoViewModel? {
        if let info = info {
            return UserInfoViewModel(info)
        } else {
            return nil
        }
    }

    var username: String {
        return self.userInfo.username
    }

    var bio: String {
        return self.userInfo.bio.characters.count == 0 ? "No bio" : self.userInfo.bio
    }

    var totalMedia: String {
        return "media\n\(self.userInfo.totalMedia)"
    }

    var totalFollows: String {
        return "follows\n\(self.userInfo.totalFollows)"
    }

    var totalFollowers: String {
        return "followers\n\(self.userInfo.totalFollowers)"
    }

    var avatar: URL {
        return self.userInfo.avatar
    }
}
