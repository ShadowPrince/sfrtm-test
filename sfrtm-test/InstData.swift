//
//  InstData.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import Foundation

// I've worked with Decodable before so I'd rather use it now
import Decodable
import UIKit

typealias Id = String

struct InstUserInfo {
    let id: Id
    let username: String
    let fullName: String
    let bio: String

    let totalMedia: Int
    let totalFollows: Int
    let totalFollowers: Int
    let avatar: URL
}

extension InstUserInfo: Decodable {
    static func decode(_ j: Any) throws -> InstUserInfo {
        return try InstUserInfo(id: j => "id",
                                username: j => "username",
                                fullName: j => "full_name",
                                bio: j => "bio",
                                totalMedia: j => "counts" => "media",
                                totalFollows: j => "counts" => "follows",
                                totalFollowers: j => "counts" => "followed_by",
                                avatar: URL(string: j => "profile_picture").tryUnwrap(with: DecodingError.invalidUrl(key: "profile_picture", obj: j))
        )
    }
}

struct InstImage {
    let size: (Int, Int)
    let url: URL
}

extension InstImage: Decodable {
    static func decode(_ j: Any) throws -> InstImage {
        return try InstImage(size: (j => "width", j => "height"),
                             url: URL(string: j => "url").tryUnwrap(with: DecodingError.invalidUrl(key: "url", obj: j))
        )
    }
}

struct InstMedia {
    enum MediaType {
        case image
        case video
    }
    
    let id: Id
    let title: String?
    let type: MediaType
    let thumbnail: InstImage
    let source: InstImage
}

extension InstMedia: Decodable {
    static func decode(_ j: Any) throws -> InstMedia {
        if (j as! NSDictionary)["caption"] as? NSNull != nil {
            print(1)
        }

        let caption = try j => "caption"
        return try InstMedia(id: j => "id",
                             title: caption is NSNull ? nil : caption => "text",
                             type: (j => "type" as? String) == Optional.init("image") ? MediaType.image : MediaType.video,
                             thumbnail: InstImage.decode(j => "images" => "thumbnail"),
                             source: InstImage.decode(j => "images" => "standard_resolution")
        )
    }
}
