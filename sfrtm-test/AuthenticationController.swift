//
//  AuthenticationController.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit

class AuthenticationController: UIViewController, UIWebViewDelegate {
    enum Segues: String {
        case proceed = "proceedSegue"
    }

    @IBOutlet weak var authWebView: UIWebView!

    override func viewDidLoad() {
        self.authWebView.loadRequest(URLRequest(url: InstAPI.authenticationUrl()))
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        guard let url = request.url else { return true }
        if InstAPI.authenticationTry(url: url) {
            self.performSegue(withIdentifier: Segues.proceed.rawValue, sender: nil)
            return false
        }

        return true
    }
}
