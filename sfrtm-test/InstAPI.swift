//
//  InstAPI.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Decodable

class InstAPI {
    typealias Token = String
    typealias Callback<T> = (T?, Swift.Error?) -> Void
    typealias Decoder<T> = (Any) throws -> T

    enum Error: Swift.Error {
        case server(code: Int)
        case invalidJson
        case decodeFailed
        case network
    }
        
    enum Endpoint: String {
        case base = "https://api.instagram.com/v1/"

        case userSelf = "users/self"
        case userSelfMedia = "users/self/media/recent"
    }

    private static var _shared: InstAPI?
    static var shared: InstAPI {
        guard let shared = self._shared else {
            // @TODO: panic
            return self._shared!
        }

        return shared
    }

    private let token: Token
    private init(token: Token) {
        self.token = token
    }

    // MARK: meta
    private func perform<T>(endpoint: Endpoint, params: [String: Any], decoder: @escaping Decoder<T>, _ callback: @escaping Callback<T>) {
        // sry alamofire
        func makeUrl(_ point: Endpoint, _ params: [String: Any]) -> URL {
            var buffer = Endpoint.base.rawValue
            buffer.append(point.rawValue)
            buffer.append("?")
            
            for (k, v) in params {
                buffer.append("\(k.escaped)=\(String(describing: v).escaped)&")
            }

            buffer.append("access_token=\(self.token)")
            return URL(string: buffer)!
        }

        let url = makeUrl(endpoint, params)
        URLSession.shared.dataTask(with: url) { (data, _, _) in
            var object: T?
            var error: Swift.Error?
            
            do {
                let data = try data.tryUnwrap(with: Error.network)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                object = try decoder(json => "data")
            } catch let e {
                error = e
            }

            OperationQueue.main.addOperation {
                callback(object, error)
            }
        }.resume()
    }

    // MARK: auth
    private static let authenticationRedirect = URL(string: "https://google.com/")!
    private static let clientId = "e8e019478ffd4243a2575927a0719b53"
    static func authenticationUrl() -> URL {
        return URL(string: "https://api.instagram.com/oauth/authorize/?client_id=\(self.clientId)&redirect_uri=\(self.authenticationRedirect.absoluteString.escaped!)&response_type=token")!
    }

    static func authenticationTry(url: URL) -> Bool {
        if let token = NSRegularExpression.firstMatch(of: ".*?#access_token=(.*)", at: url.absoluteString) {
            InstAPI._shared = InstAPI(token: token)
            return true
        }

        return false
    }

    // MARK: user
    func selfInfo(_ cb: @escaping Callback<InstUserInfo>) {
        self.perform(endpoint: .userSelf, params: [:], decoder: InstUserInfo.decode, cb)
    }

    // MARK: media
    func selfMedia(_ cb: @escaping Callback<Array<InstMedia>>) {
        self.perform(endpoint: .userSelfMedia, params: [:], decoder: Array.decoder(InstMedia.decode), cb)
    }
}
