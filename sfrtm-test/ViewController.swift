//
//  ViewController.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/26/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController, UIScrollViewDelegate {
    var media: InstMedia!

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        ImageCache.default.retrieveImage(forKey: self.media.thumbnail.url.absoluteString, options: nil) { cached, _ in
            self.imageView.kf.setImage(with: self.media.source.url, placeholder: cached)
        }
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    @IBAction func closeGestureAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareGestureAction(_ sender: Any) {
        let activity = UIActivityViewController(activityItems: [self.media.source.url],
                                                applicationActivities: [])
        self.present(activity, animated: true, completion: nil)
    }
}
