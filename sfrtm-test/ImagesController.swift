//
//  ImagesController.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Kingfisher

class ImagesController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    var shouldBlur = true
    var media: [InstMedia] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        InstAPI.shared.selfMedia { media, error in
            guard let media = media else {
                let alert = UIAlertController.init(title: "Error", message: "Failed to get media from the server: \(error)", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                return
            }

            self.media.append(contentsOf: media)
            self.collectionView.reloadData()
            self.collectionView.unblurIt()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.shouldBlur {
            self.collectionView.blurIt()
        }
        
        self.shouldBlur = false
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.media.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        let media = self.media[indexPath.row]

        imageView.kf.indicatorType = .activity
        imageView.kf.indicatorType = .custom(indicator: GlowIndicator())
        imageView.kf.setImage(with: media.thumbnail.url)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performChainAction(#selector(ProfileController.selectMediaAction(_:)), sender: self.media[indexPath.row])
    }
}
