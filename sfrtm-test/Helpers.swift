//
//  Helpers.swift
//  sfrtm-test
//
//  Created by shdwprince on 3/25/17.
//  Copyright © 2017 shdwprince. All rights reserved.
//

import UIKit
import Decodable

enum OptionalUnwrapError: Error {
    case error
}

extension Optional {
    func tryUnwrap(with error: Error) throws -> Wrapped {
        guard let value = self else {
            throw error
        }
        
        return value
    }
}

extension String {
    var escaped: String? {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
    }
}

extension NSRegularExpression {
    static func firstMatch(of regex: String, at string: String) -> String? {
        guard let regex = try? NSRegularExpression(pattern: regex, options: []) else {
            return nil
        }
        
        let matches = regex.matches(in: string, options: [], range: NSMakeRange(0, string.characters.count))
        if matches.count >= 1 {
            return (string as NSString).substring(with: matches[0].rangeAt(1))
        }

        return nil
    }
}

extension DecodingError {
    static func invalidUrl(key: String, obj: Any) -> DecodingError {
        return DecodingError.rawRepresentableInitializationError(rawValue: key, DecodingError.Metadata(object: obj))
    }
}

extension UIResponder {
    func performChainAction(_ sel: Selector, sender: Any) {
        var responder = self
        while let next = responder.next {
            if responder.responds(to: sel) {
                responder.perform(sel, with: sender)
                return
            }

            responder = next
        }
    }
}
